#### Image filter

    Usage: PATHS | imf FILTER1 FILTER2 FILTERn SORTING

    FILTER FORMAT:
    LOR
    ││╰─▸ RHS
    │╰──▸ OPERATOR
    ╰───▸ LHS

    AVAILABLE FILTERS:
    LHS([w]idth)      OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER | [h]eight)
    LHS([h]eight)     OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER | [w]idth)
    LHS([d]elta)      OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([l]ong_side)  OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([s]hort_side) OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([a]rea)       OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([p]roportion) OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | RATIONAL_NUMBER)

    SORTING FORMAT:
    DL
    │╰──▸ LHS
    ╰───▸ DIRECTION: [r]ise or [f]all
