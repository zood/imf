use std::path::PathBuf;

use rational::Rational;

use crate::{
	pool::Pool,
	stats::{self, Common},
};

#[derive(Clone)]
pub struct Img {
	pub path: PathBuf,
	pub width: usize,
	pub height: usize,
}

impl Img {
	pub fn new(path: PathBuf, width: usize, height: usize) -> Self {
		Self {
			path,
			width,
			height,
		}
	}

	pub fn delta(&self) -> usize {
		self.long_side() - self.short_side()
	}

	pub fn long_side(&self) -> usize {
		[self.width, self.height].into_iter().max().unwrap()
	}

	pub fn short_side(&self) -> usize {
		[self.width, self.height].into_iter().min().unwrap()
	}

	pub fn area(&self) -> usize {
		self.width * self.height
	}

	pub fn proportion(&self) -> Rational {
		Rational::new(self.short_side() as u64, self.long_side() as u64)
	}
}

pub enum RelRule {
	Width(fn(usize, usize) -> bool),
	Height(fn(usize, usize) -> bool),
	Delta(fn(usize, usize) -> bool),
	LongSide(fn(usize, usize) -> bool),
	ShortSide(fn(usize, usize) -> bool),
	Area(fn(usize, usize) -> bool),
	Proportion(fn(Rational, Rational) -> bool),
}

pub enum AbsRule {
	WH(Box<dyn Fn(usize, usize) -> bool>),
	Delta(Box<dyn Fn(usize) -> bool>),
	LongSide(Box<dyn Fn(usize) -> bool>),
	ShortSide(Box<dyn Fn(usize) -> bool>),
	Area(Box<dyn Fn(usize) -> bool>),
	Proportion(Box<dyn Fn(Rational) -> bool>),
}

#[derive(PartialEq, Eq, Debug)]
pub enum Measure {
	Width,
	Height,
	Delta,
	LongSide,
	ShortSide,
	Area,
	Proportion,
}

#[derive(PartialEq, Eq, Debug)]
pub enum Sorting {
	Rise(Measure),
	Fall(Measure),
}

pub struct AbsFilter(pub AbsRule);
impl AbsFilter {
	pub fn test(&self, img: &Img) -> bool {
		match &self.0 {
			AbsRule::WH(rule) => rule(img.width, img.height),
			AbsRule::Delta(rule) => rule(img.delta()),
			AbsRule::LongSide(rule) => rule(img.long_side()),
			AbsRule::ShortSide(rule) => rule(img.short_side()),
			AbsRule::Area(rule) => rule(img.area()),
			AbsRule::Proportion(rule) => rule(img.proportion()),
		}
	}
}

pub struct RelFilter(pub RelRule);
impl RelFilter {
	pub fn common_measure(&self, pool: &Pool) -> Option<Common> {
		match self.0 {
			RelRule::Width(_) => stats::common_width(pool),
			RelRule::Height(_) => stats::common_height(pool),
			RelRule::Delta(_) => stats::common_delta(pool),
			RelRule::LongSide(_) => stats::common_long_side(pool),
			RelRule::ShortSide(_) => stats::common_short_side(pool),
			RelRule::Area(_) => stats::common_area(pool),
			RelRule::Proportion(_) => stats::common_proportion(pool),
		}
	}

	pub fn test(&self, img: &Img, common: Common) -> bool {
		match self.0 {
			RelRule::Width(rule) => rule(img.width, common.usize()),
			RelRule::Height(rule) => rule(img.height, common.usize()),
			RelRule::Delta(rule) => rule(img.delta(), common.usize()),
			RelRule::LongSide(rule) => rule(img.long_side(), common.usize()),
			RelRule::ShortSide(rule) => rule(img.short_side(), common.usize()),
			RelRule::Area(rule) => rule(img.area(), common.usize()),
			RelRule::Proportion(rule) => rule(img.proportion(), common.rational()),
		}
	}
}

macro_rules! wh {
	($e:expr) => {
		AbsRule::WH(Box::new($e))
	};
}

fn equal<T: PartialEq>(measure: T, common: T) -> bool {
	common == measure
}

fn nonequal<T: PartialEq>(measure: T, common: T) -> bool {
	common != measure
}

peg::parser!(pub grammar parse() for str {
	pub rule rel_rule() -> RelRule
		= r:wnorm() { r }
		/ r:hnorm() { r }
		/ r:dnorm() { r }
		/ r:lnorm() { r }
		/ r:snorm() { r }
		/ r:anorm() { r }
		/ r:pnorm() { r }

	pub rule abs_rule() -> AbsRule
		= r:wh() { r }
		/ r:wn() { r }
		/ r:hn() { r }
		/ r:dn() { r }
		/ r:ln() { r }
		/ r:sn() { r }
		/ r:an() { r }
		/ r:pn() { r }

	rule wh() -> AbsRule
		= "w=h"  { wh!(|w,h| w == h) }
		/ "h=w"  { wh!(|w,h| w == h) }
		/ "w>h"  { wh!(|w,h| w > h) }
		/ "h<w"  { wh!(|w,h| w > h) }
		/ "w<h"  { wh!(|w,h| w < h) }
		/ "h>w"  { wh!(|w,h| w < h) }
		/ "w>=h" { wh!(|w,h| w >= h) }
		/ "h<=w" { wh!(|w,h| w >= h) }
		/ "w<=h" { wh!(|w,h| w <= h) }
		/ "h>=w" { wh!(|w,h| w <= h) }

	rule natural() -> usize = n:$(['1'..='9']['0'..='9']*) { n.parse::<usize>().unwrap() }
	rule rational() -> Rational = a:natural() "/" b:natural() {
		let (min, max) = if a > b {
			(b, a)
		} else {
			(a, b)
		};
		Rational::new(min as u8, max as u8)
	}

	rule wn() -> AbsRule
		= "w=" n:natural() { wh!(move |w,_| w == n) }
		/ "w!=" n:natural() { wh!(move |w,_| w != n) }
		/ "w>" n:natural() { wh!(move |w,_| w > n) }
		/ "w>=" n:natural() { wh!(move |w,_| w >= n) }
		/ "w<" n:natural() { wh!(move |w,_| w < n) }
		/ "w<=" n:natural() { wh!(move |w,_| w <= n) }
	rule hn() -> AbsRule
		= "h=" n:natural() { wh!(move |_,h| h == n) }
		/ "h!=" n:natural() { wh!(move |_,h| h != n) }
		/ "h>" n:natural() { wh!(move |_,h| h > n) }
		/ "h>=" n:natural() { wh!(move |_,h| h >= n) }
		/ "h<" n:natural() { wh!(move |_,h| h < n) }
		/ "h<=" n:natural() { wh!(move |_,h| h <= n) }
	rule dn() -> AbsRule
		= "d=" n:natural() { AbsRule::Delta(Box::new(move |x| x == n)) }
		/ "d!=" n:natural() { AbsRule::Delta(Box::new(move |x| x != n)) }
		/ "d>" n:natural() { AbsRule::Delta(Box::new(move |x| x > n)) }
		/ "d>=" n:natural() { AbsRule::Delta(Box::new(move |x| x >= n)) }
		/ "d<" n:natural() { AbsRule::Delta(Box::new(move |x| x < n)) }
		/ "d<=" n:natural() { AbsRule::Delta(Box::new(move |x| x <= n)) }
	rule ln() -> AbsRule
		= "l=" n:natural() { AbsRule::LongSide(Box::new(move |x| x == n)) }
		/ "l!=" n:natural() { AbsRule::LongSide(Box::new(move |x| x != n)) }
		/ "l>" n:natural() { AbsRule::LongSide(Box::new(move |x| x > n)) }
		/ "l>=" n:natural() { AbsRule::LongSide(Box::new(move |x| x >= n)) }
		/ "l<" n:natural() { AbsRule::LongSide(Box::new(move |x| x < n)) }
		/ "l<=" n:natural() { AbsRule::LongSide(Box::new(move |x| x <= n)) }
	rule sn() -> AbsRule
		= "s=" n:natural() { AbsRule::ShortSide(Box::new(move |x| x == n)) }
		/ "s!=" n:natural() { AbsRule::ShortSide(Box::new(move |x| x != n)) }
		/ "s>" n:natural() { AbsRule::ShortSide(Box::new(move |x| x > n)) }
		/ "s>=" n:natural() { AbsRule::ShortSide(Box::new(move |x| x >= n)) }
		/ "s<" n:natural() { AbsRule::ShortSide(Box::new(move |x| x < n)) }
		/ "s<=" n:natural() { AbsRule::ShortSide(Box::new(move |x| x <= n)) }
	rule an() -> AbsRule
		= "a=" n:natural() { AbsRule::Area(Box::new(move |x| x == n)) }
		/ "a!=" n:natural() { AbsRule::Area(Box::new(move |x| x != n)) }
		/ "a>" n:natural() { AbsRule::Area(Box::new(move |x| x > n)) }
		/ "a>=" n:natural() { AbsRule::Area(Box::new(move |x| x >= n)) }
		/ "a<" n:natural() { AbsRule::Area(Box::new(move |x| x < n)) }
		/ "a<=" n:natural() { AbsRule::Area(Box::new(move |x| x <= n)) }
	rule pn() -> AbsRule
		= "p=" n:rational() { AbsRule::Proportion(Box::new(move |x| x == n)) }
		/ "p!=" n:rational() { AbsRule::Proportion(Box::new(move |x| x != n)) }
		/ "p>" n:rational() { AbsRule::Proportion(Box::new(move |x| x > n)) }
		/ "p>=" n:rational() { AbsRule::Proportion(Box::new(move |x| x >= n)) }
		/ "p<" n:rational() { AbsRule::Proportion(Box::new(move |x| x < n)) }
		/ "p<=" n:rational() { AbsRule::Proportion(Box::new(move |x| x <= n)) }

	rule wnorm() -> RelRule
		= "w=c" { RelRule::Width(equal) }
		/ "w=u" { RelRule::Width(nonequal) }
	rule hnorm() -> RelRule
		= "h=c" { RelRule::Height(equal) }
		/ "h=u" { RelRule::Height(nonequal) }
	rule dnorm() -> RelRule
		= "d=c" { RelRule::Delta(equal) }
		/ "d=u" { RelRule::Delta(nonequal) }
	rule lnorm() -> RelRule
		= "l=c" { RelRule::LongSide(equal) }
		/ "l=u" { RelRule::LongSide(nonequal) }
	rule snorm() -> RelRule
		= "s=c" { RelRule::ShortSide(equal) }
		/ "s=u" { RelRule::ShortSide(nonequal) }
	rule anorm() -> RelRule
		= "a=c" { RelRule::Area(equal) }
		/ "a=u" { RelRule::Area(nonequal) }
	rule pnorm() -> RelRule
		= "p=c" { RelRule::Proportion(equal) }
		/ "p=u" { RelRule::Proportion(nonequal) }

	pub rule sorting() -> Sorting
		= "rw" { Sorting::Rise(Measure::Width) }
		/ "fw" { Sorting::Fall(Measure::Width) }
		/ "rh" { Sorting::Rise(Measure::Height) }
		/ "fh" { Sorting::Fall(Measure::Height) }
		/ "rd" { Sorting::Rise(Measure::Delta) }
		/ "fd" { Sorting::Fall(Measure::Delta) }
		/ "rl" { Sorting::Rise(Measure::LongSide) }
		/ "fl" { Sorting::Fall(Measure::LongSide) }
		/ "rs" { Sorting::Rise(Measure::ShortSide) }
		/ "fs" { Sorting::Fall(Measure::ShortSide) }
		/ "ra" { Sorting::Rise(Measure::Area) }
		/ "fa" { Sorting::Fall(Measure::Area) }
		/ "rp" { Sorting::Rise(Measure::Proportion) }
		/ "fp" { Sorting::Fall(Measure::Proportion) }
});

#[cfg(test)]
mod tests {
	use super::*;

	macro_rules! img {
		($w:literal, $h:literal) => {
			Img::new(PathBuf::from("/some/path"), $w, $h)
		};
	}

	macro_rules! abs_filter {
		($filters:literal) => {
			AbsFilter(parse::abs_rule($filters).unwrap())
		};
	}

	macro_rules! rel_filter {
		($filter:literal) => {
			RelFilter(parse::rel_rule($filter).unwrap())
		};
	}

	macro_rules! pool {
        ($($item:expr),*) => {{
            let mut v = Vec::new();
            $(v.push($item.clone());)*
            Pool(v)
        }};
    }

	#[test]
	fn test_weqh() {
		let filter1 = abs_filter!("w=h");
		let filter2 = abs_filter!("h=w");
		let img = img!(100, 100);
		assert_eq!(filter1.test(&img), filter2.test(&img));
	}
	#[test]
	fn test_wgth() {
		let filter1 = abs_filter!("w>h");
		let filter2 = abs_filter!("h<w");
		let img = img!(200, 100);
		assert_eq!(filter1.test(&img), filter2.test(&img));
	}
	#[test]
	fn test_wgeh() {
		let filter1 = abs_filter!("w>=h");
		let filter2 = abs_filter!("h<=w");
		let img = img!(200, 100);
		assert_eq!(filter1.test(&img), filter2.test(&img));
	}
	#[test]
	fn test_wlth() {
		let filter1 = abs_filter!("w<h");
		let filter2 = abs_filter!("h>w");
		let img = img!(100, 200);
		assert_eq!(filter1.test(&img), filter2.test(&img));
	}
	#[test]
	fn test_wleh() {
		let filter1 = abs_filter!("w<=h");
		let filter2 = abs_filter!("h>=w");
		let img = img!(100, 200);
		assert_eq!(filter1.test(&img), filter2.test(&img));
	}
	#[test]
	fn test_wn() {
		let filter = abs_filter!("w=111");
		let img = img!(111, 50);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_hn() {
		let filter = abs_filter!("h=55");
		let img = img!(111, 55);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_dn() {
		let filter = abs_filter!("d=20");
		let img = img!(100, 80);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_ln() {
		let filter = abs_filter!("l=90");
		let img = img!(90, 80);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_sn() {
		let filter = abs_filter!("s<80");
		let img = img!(90, 70);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_sa() {
		let filter = abs_filter!("a=900");
		let img = img!(30, 30);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_pn() {
		let filter = abs_filter!("p=1/2");
		let img = img!(50, 100);
		assert!(filter.test(&img));
	}
	#[test]
	fn test_wnorm_comm() {
		let filter = rel_filter!("w=c");
		let img1 = img!(20, 60);
		let img2 = img!(20, 70);
		let pool = pool!(img2, img!(70, 80), img1);
		let common = filter.common_measure(&pool);
		assert!(filter.test(&img1, common.unwrap()));
		assert!(filter.test(&img2, common.unwrap()));
	}
	#[test]
	fn test_wnorm_uncomm() {
		let filter = rel_filter!("w=u");
		let img1 = img!(20, 60);
		let img2 = img!(20, 70);
		let img3 = img!(70, 80);
		let pool = pool!(img2, img3, img1);
		let common = filter.common_measure(&pool);
		assert!(!filter.test(&img1, common.unwrap()));
		assert!(!filter.test(&img2, common.unwrap()));
		assert!(filter.test(&img3, common.unwrap()));
	}
	#[test]
	fn test_wnorm_noncomm() {
		let filter = rel_filter!("w=c");
		let img1 = img!(20, 60);
		let img2 = img!(30, 70);
		let pool = pool!(img1, img2);
		let common = filter.common_measure(&pool);
		assert!(common.is_none());
	}
	#[test]
	fn test_hnorm_comm() {
		let filter = rel_filter!("h=c");
		let img1 = img!(60, 20);
		let img2 = img!(70, 20);
		let pool = pool!(img2, img!(70, 80), img1);
		let common = filter.common_measure(&pool);
		assert!(filter.test(&img1, common.unwrap()));
		assert!(filter.test(&img2, common.unwrap()));
	}
	#[test]
	fn test_pnorm_comm() {
		let filter = rel_filter!("p=c");
		let img1 = img!(20, 60);
		let img2 = img!(30, 90);
		let img3 = img!(70, 80);
		let pool = pool!(img1, img2, img3);
		let common = filter.common_measure(&pool);
		assert!(filter.test(&img1, common.unwrap()));
		assert!(filter.test(&img2, common.unwrap()));
		assert!(!filter.test(&img3, common.unwrap()));
	}
	#[test]
	fn test_sorting() {
		assert_eq!(parse::sorting("rw"), Ok(Sorting::Rise(Measure::Width)));
		assert_eq!(parse::sorting("fw"), Ok(Sorting::Fall(Measure::Width)));
		assert_eq!(parse::sorting("rh"), Ok(Sorting::Rise(Measure::Height)));
		assert_eq!(parse::sorting("fh"), Ok(Sorting::Fall(Measure::Height)));
		assert_eq!(parse::sorting("rd"), Ok(Sorting::Rise(Measure::Delta)));
		assert_eq!(parse::sorting("fd"), Ok(Sorting::Fall(Measure::Delta)));
		assert_eq!(parse::sorting("rl"), Ok(Sorting::Rise(Measure::LongSide)));
		assert_eq!(parse::sorting("fl"), Ok(Sorting::Fall(Measure::LongSide)));
		assert_eq!(parse::sorting("rs"), Ok(Sorting::Rise(Measure::ShortSide)));
		assert_eq!(parse::sorting("fs"), Ok(Sorting::Fall(Measure::ShortSide)));
		assert_eq!(parse::sorting("ra"), Ok(Sorting::Rise(Measure::Area)));
		assert_eq!(parse::sorting("fa"), Ok(Sorting::Fall(Measure::Area)));
		assert_eq!(parse::sorting("rp"), Ok(Sorting::Rise(Measure::Proportion)));
		assert_eq!(parse::sorting("fp"), Ok(Sorting::Fall(Measure::Proportion)));
	}
}
