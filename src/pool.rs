use core::cmp::Reverse;

use crate::cli::{Img, Measure, Sorting};

#[derive(Clone)]
pub struct Pool(pub Vec<Img>);

impl Pool {
	pub fn images(&self) -> &[Img] {
		&self.0
	}

	pub fn push(&mut self, img: Img) {
		self.0.push(img);
	}

	pub fn retain<F>(&mut self, f: F)
	where F: Fn(&Img) -> bool {
		self.0.retain(|i| f(i))
	}

	pub fn sort(&mut self, sorting: &Sorting) {
		match sorting {
			Sorting::Rise(Measure::Width) => self.0.sort_by_key(|i| i.width),
			Sorting::Fall(Measure::Width) => self.0.sort_by_key(|i| Reverse(i.width)),
			Sorting::Rise(Measure::Height) => self.0.sort_by_key(|i| i.height),
			Sorting::Fall(Measure::Height) => self.0.sort_by_key(|i| Reverse(i.height)),
			Sorting::Rise(Measure::Delta) => self.0.sort_by_key(|i| i.delta()),
			Sorting::Fall(Measure::Delta) => self.0.sort_by_key(|i| Reverse(i.delta())),
			Sorting::Rise(Measure::LongSide) => self.0.sort_by_key(|i| i.long_side()),
			Sorting::Fall(Measure::LongSide) => self.0.sort_by_key(|i| Reverse(i.long_side())),
			Sorting::Rise(Measure::ShortSide) => self.0.sort_by_key(|i| i.short_side()),
			Sorting::Fall(Measure::ShortSide) => self.0.sort_by_key(|i| Reverse(i.short_side())),
			Sorting::Rise(Measure::Area) => self.0.sort_by_key(|i| i.area()),
			Sorting::Fall(Measure::Area) => self.0.sort_by_key(|i| Reverse(i.area())),
			Sorting::Rise(Measure::Proportion) => self.0.sort_by_key(|i| i.proportion()),
			Sorting::Fall(Measure::Proportion) => self.0.sort_by_key(|i| Reverse(i.proportion())),
		}
	}
}
