use std::{
	io::{self, BufReader, BufWriter, IsTerminal, Write},
	os::unix::ffi::OsStrExt,
	path::Path,
};

use cli::Img;
use smallvec::SmallVec;

mod cli;
mod io_ext;
mod pool;
mod stats;

use io_ext::BufReadExt;
use pool::Pool;

const BELL: &str = "\x07";
const BUFSIZE: usize = 8 * 1024;
const HELP: &str = "\
Usage: PATHS | imf FILTER1 FILTER2 FILTERn SORTING

FILTER FORMAT:
    LOR
    ││╰─▸ RHS
    │╰──▸ OPERATOR
    ╰───▸ LHS

AVAILABLE FILTERS:
    LHS([w]idth)      OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER | [h]eight)
    LHS([h]eight)     OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER | [w]idth)
    LHS([d]elta)      OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([l]ong_side)  OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([s]hort_side) OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([a]rea)       OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | NATURAL_NUMBER)
    LHS([p]roportion) OPERATOR(= != < <= > >=) RHS([c]ommon | [u]ncommon | RATIONAL_NUMBER)

SORTING FORMAT:
    DL
    │╰──▸ LHS
    ╰───▸ DIRECTION: [r]ise or [f]all";

fn abort(msg: impl std::fmt::Display) {
	eprintln!("{msg}");
	std::process::exit(1);
}

fn writeln(writer: &mut impl Write, path: &Path) -> io::Result<()> {
	writer.write_all(path.as_os_str().as_bytes())?;
	writer.write_all(b"\n")?;
	Ok(())
}

#[derive(Default)]
struct Actions {
	abs_filters: SmallVec<cli::AbsFilter, 4>,
	rel_filters: SmallVec<cli::RelFilter, 4>,
	sorting: SmallVec<cli::Sorting, 1>,
}

impl Actions {
	fn push_abs_filter(&mut self, filter: cli::AbsFilter) {
		self.abs_filters.push(filter);
	}
	fn push_rel_filter(&mut self, filter: cli::RelFilter) {
		self.rel_filters.push(filter);
	}
	fn set_sorting(&mut self, sorting: cli::Sorting) {
		match self.sorting.len() {
			0 => self.sorting.push(sorting),
			1 => self.sorting[0] = sorting,
			_ => unreachable!(),
		}
	}
	fn len(&self) -> usize {
		self.abs_filters.len() + self.rel_filters.len() + self.sorting.len()
	}
	fn is_empty(&self) -> bool {
		self.abs_filters.is_empty() && self.rel_filters.is_empty() && self.sorting.is_empty()
	}
}

fn main() {
	let args: Vec<_> = std::env::args().skip(1).collect();
	let mut actions = Actions::default();
	for arg in args {
		if let Ok(abs_rule) = cli::parse::abs_rule(&arg) {
			let abs_filter = cli::AbsFilter(abs_rule);
			actions.push_abs_filter(abs_filter);
		} else if let Ok(rel_rule) = cli::parse::rel_rule(&arg) {
			let rel_filter = cli::RelFilter(rel_rule);
			actions.push_rel_filter(rel_filter);
		} else if let Ok(sorting) = cli::parse::sorting(&arg) {
			actions.set_sorting(sorting);
		}
	}

	if actions.is_empty() || io::stdin().is_terminal() {
		abort(HELP);
	}

	let reader = BufReader::with_capacity(BUFSIZE, io::stdin());
	let mut writer = BufWriter::with_capacity(BUFSIZE, io::stdout());

	if actions.len() == actions.abs_filters.len() {
		for path in reader.path_lines().flatten() {
			match imagesize::size(&path) {
				Ok(dim) => {
					let img = Img::new(path, dim.width, dim.height);
					if actions.abs_filters.iter().all(|f| f.test(&img)) {
						let _ = writeln(&mut writer, &img.path);
					}
				}
				Err(e) => eprintln!("{}[imf] ERROR: {}: {}", BELL, path.display(), e),
			}
		}
	} else {
		let mut pool = Pool(Vec::with_capacity(128));
		for path in reader.path_lines().flatten() {
			match imagesize::size(&path) {
				Ok(dim) => {
					let img = Img::new(path, dim.width, dim.height);
					pool.push(img);
				}
				Err(e) => eprintln!("{}[imf] ERROR: {}: {}", BELL, path.display(), e),
			}
		}

		actions
			.abs_filters
			.iter()
			.for_each(|filter| pool.retain(|i| filter.test(i)));

		for rel_filter in actions.rel_filters {
			match rel_filter.common_measure(&pool) {
				Some(common) => pool.retain(|i| rel_filter.test(i, common)),
				None => return,
			}
		}

		if let Some(sorting) = actions.sorting.first() {
			pool.sort(sorting);
		}

		let _ = pool
			.images()
			.iter()
			.try_for_each(|i| writeln(&mut writer, &i.path));
	}
}
