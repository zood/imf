use ahash::AHashMap as HashMap;
use rational::Rational;

use crate::pool::Pool;

#[derive(Copy, Clone)]
pub enum Common {
	Usize(usize),
	Rational(Rational),
}

impl Common {
	pub fn usize(self) -> usize {
		match self {
			Common::Usize(n) => n,
			_ => unreachable!(),
		}
	}

	pub fn rational(self) -> Rational {
		match self {
			Common::Rational(n) => n,
			_ => unreachable!(),
		}
	}
}

pub fn common_width(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.width).or_insert(0_usize);
		*c += 1
	}
	let (width, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(width));
	}
	None
}

pub fn common_height(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.height).or_insert(0_usize);
		*c += 1
	}
	let (height, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(height));
	}
	None
}

pub fn common_delta(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.delta()).or_insert(0_usize);
		*c += 1
	}
	let (delta, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(delta));
	}
	None
}

pub fn common_long_side(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.long_side()).or_insert(0_usize);
		*c += 1
	}
	let (long_side, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(long_side));
	}
	None
}

pub fn common_short_side(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.short_side()).or_insert(0_usize);
		*c += 1
	}
	let (short_side, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(short_side));
	}
	None
}

pub fn common_area(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.area()).or_insert(0_usize);
		*c += 1
	}
	let (area, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Usize(area));
	}
	None
}

pub fn common_proportion(pool: &Pool) -> Option<Common> {
	let mut table = HashMap::new();
	for img in pool.images() {
		let c = table.entry(img.proportion()).or_insert(0_usize);
		*c += 1
	}
	let (proportion, freq) = table.into_iter().max_by_key(|(_, f)| *f).unwrap();
	if freq * 2 > pool.images().len() {
		return Some(Common::Rational(proportion));
	}
	None
}
